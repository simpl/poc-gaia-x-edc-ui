export interface Prefix {
  alias: string;
  url: string;
}

export interface Path {
  prefix: string;
  value: string;
}

export interface Description {
  en: string;
}

export interface Validation {
  key: string;
  value: string;
}

export interface Constraint {
  path: Path;
  name: string;
  datatype: {
    prefix: string;
    value: string;
  };
  minCount?: number;
  maxCount?: number;
  description?: Description;
  example?: string;
  in?: { value: string }[];
  order?: number;
  validations?: Validation[];
  children?: string;
}

export interface Shape {
  schema: string;
  targetClassPrefix: string;
  targetClassName: string;
  constraints: Constraint[];
  children?: string;
}

export interface ServiceShapesData {
  prefixList: Prefix[];
  shapes: Shape[];
  shapeFileName: string;
}
