/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
  theme: {
    extend: {
      colors: {
        simpl: {
          'dark-blue': '#002B6E',
          'light-blue': '#51E7F9',
          'dark-pink': '#B60F8D',
          turquoise: '#01BBA6',
        },
        ec: {
          primary: {
            DEFAULT: '#3860ED',
            180: '#051036',
            160: '#0A1F6C',
            140: '#0F2FA2',
            120: '#143FD9',
            100: '#3860ED', // Primary
            80: '#5577F0',
            60: '#89A1F4',
            40: '#B1C0F8',
            20: '#D8E0FB',
          },
          secondary: {
            DEFAULT: '#FFBE5C',
            180: '#8F5600',
            160: '#E08700',
            140: '#FF9D0A',
            120: '#FFAD33',
            100: '#FFBE5C', // Secondary
            80: '#FFCB7C',
            60: '#FFD89D',
            40: '#FFE5BE',
            20: '#FFF2DE',
          },
          dark: {
            DEFAULT: '#26324B',
            100: '#26324B', // Dark
            80: '#546FA6',
            60: '#99AACC',
          },
          status: {
            info: '#3860ED',
            success: '#24A148',
            warning: '#F39811',
            error: '#DA1E28',
          },
          accent: {
            DEFAULT: '#BBB3FF',
            160: '#887DE8',
            140: '#978CF2',
            120: '#A89EFA',
            100: '#BBB3FF', // Accent
            80: '#BFB2FF',
            60: '#D1CCFF',
            40: '#FAFAFF',
          },
          neutral: {
            DEFAULT: '#B9C5E9',
            180: '#6C85D1',
            160: '#7F95D7',
            140: '#92A5DD',
            120: '#A6B5E3',
            100: '#B9C5E9', // Neutral
            80: '#CDD5EF',
            60: '#E0E5F5',
            40: '#F3F5FB',
            20: '#F8F9FD',
          },
          other: {
            background: '#FCFCFC',
            branding: '#004494',
            white: '#FFFFFF',
          },
        },
      },
    },
  },
  plugins: [],
};
