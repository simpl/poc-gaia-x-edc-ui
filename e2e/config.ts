export const BASE_URL = 'http://poc-ui.dev.simpl-europe.eu';
export const SD_TOOL_URL =
  'https://simplUser:2MYgj84U8xS4h3@creation-wizard-frontend.dev.simpl-europe.eu';

type SelectorGroup = Record<string, string> & { tag: string };

export const dataOfferingFormSelectors: Record<string, SelectorGroup> = {
  generalServiceProperties: {
    tag: 'mat-accordion:has-text("general service properties -")',
    identifier: 'input[id="mat-input-0"]',
    title: 'input[id="mat-input-1"]',
    description: 'input[id="mat-input-2"]',
    location: 'input[id="mat-input-3"]',
    keywords: 'input[id="mat-input-4"]',
    language: 'input[id="mat-input-5"]',
  },
  dataProperties: {
    tag: 'mat-accordion:has-text("data properties -")',
    provenance: 'input[id="mat-input-6"]',
    format: 'input[id="mat-input-7"]',
  },
  provider: {
    tag: 'mat-accordion:has-text("provider information -")',
    provider: 'input[id="mat-input-14"]',
    contact: 'input[id="mat-input-15"]',
  },
  offering: {
    tag: 'mat-accordion:has-text("offering price -")',
    license: 'input[id="mat-input-17"]',
    price: 'input[id="mat-input-18"]',
  },
};
