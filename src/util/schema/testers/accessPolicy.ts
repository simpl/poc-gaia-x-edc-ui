import { rankWith, scopeEndsWith } from '@jsonforms/core';

export const accessPolicyFieldName = 'simpl:access-policy';

export default rankWith(
  3, //increase rank as needed
  scopeEndsWith(accessPolicyFieldName)
);
