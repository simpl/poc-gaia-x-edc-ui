import type { JsonSchema4 } from '@jsonforms/core';
import jsonld from 'jsonld';
import { getAccessPolicyJsonLd } from '@/services/creationWizard';
import type { AccessPolicyPermission } from 'types/accessPolicy';

type FormDataType =
  | string
  | number
  | Record<string, string | object | number | Array<FormDataType>>
  | Array<FormDataType>;

export interface ExtendedJsonSchema4 extends JsonSchema4 {
  rdfType?: string;
}

const isStringOrNumberProperty = (schema: ExtendedJsonSchema4) => {
  return schema.rdfType === 'xsd:string' || schema.rdfType === 'xsd:number';
};

const generateRandomBase64 = (length: number): string => {
  const buffer = new Uint8Array(Math.ceil((length * 3) / 4));
  crypto.getRandomValues(buffer);
  return btoa(String.fromCharCode(...buffer))
    .slice(0, length)
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
};

const parseDataProperty = (
  data: Record<string, FormDataType>,
  schema: ExtendedJsonSchema4,
  context: Record<string, string>
) => {
  if (schema.type === 'object') {
    const formattedData: Record<string, unknown> = {
      'rdf:type': {
        '@id': schema.rdfType as string,
      },
    };
    Object.keys(data).forEach((propertyKey) => {
      const propertyValue = data[propertyKey];
      if (!schema.properties || !schema.properties[propertyKey]) {
        throw new Error('Property not found in schema');
      }
      formattedData[propertyKey] = parseDataProperty(
        propertyValue as Record<string, FormDataType>,
        schema.properties[propertyKey],
        context
      );
    });
    return formattedData;
  }
  if (schema.type === 'array' && Array.isArray(data)) {
    const formattedData: Array<unknown> = [];

    data.forEach((element, index) => {
      if (!schema.items) {
        throw new Error('Items not defined in schema');
      }

      // TODO: items can be array in very rare ocasion when values are 'fixed'
      formattedData.push(parseDataProperty(element, schema.items as ExtendedJsonSchema4, context));
    });
    return formattedData;
  } else {
    if (isStringOrNumberProperty(schema)) {
      return data;
    } else {
      return {
        '@value': data,
        '@type': schema.rdfType,
      };
    }
  }
};

// currently going at it manual, can maybe do it with n3 or rdfjs

export const formatDataToJsonLd = async (
  data: Record<string, unknown>,
  schema: ExtendedJsonSchema4,
  context: Record<string, string>
) => {
  const typeWithoutPrefix = schema.rdfType?.split(':')[1];

  // TODO: we probably shouldn't generate this everytime, also, what's the point of it ? need to investigate
  const randomBase64forId = generateRandomBase64(36);

  const jsonFormattedData: Record<string, unknown> = {
    '@context': context,
    '@id': `did:web:registry.gaia-x.eu:${typeWithoutPrefix}:${randomBase64forId}`,
    'rdf:type': {
      '@id': schema.rdfType as string,
    },
  };

  Object.keys(data).forEach(async (propertyKey) => {
    if (propertyKey === 'simpl:access-policy') {
      const policyBody = {
        resourceUri: randomBase64forId,
        permissions: JSON.parse(data[propertyKey] as string) as AccessPolicyPermission[],
      };
      try {
        const odrlPolicy = await getAccessPolicyJsonLd(policyBody);
        data[propertyKey] = JSON.stringify(odrlPolicy);
      } catch (e) {
        throw new Error('Error while fetching access policy jsonld');
      }
    }

    const propertyValue = data[propertyKey];
    if (!schema.properties || !schema.properties[propertyKey]) {
      throw new Error('Property not found in schema');
    }
    jsonFormattedData[propertyKey] = parseDataProperty(
      propertyValue as Record<string, FormDataType>,
      schema.properties[propertyKey],
      context
    );
  });

  const formattedJson = jsonld.compact(jsonFormattedData, context);
  return formattedJson;
};
