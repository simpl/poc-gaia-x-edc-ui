export const getFileExtension = (fileName: string) =>
  fileName.match(/(\.(?<extension>[^.]*?$))/)?.groups?.extension ?? '';
