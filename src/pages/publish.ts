import { uploadSelfDescription } from '@/services/federatedCatalogue';
import type { APIRoute } from 'astro';

const signJson = async (json: object) => {
  // TODO: use environment variables to point to signer
  return fetch('http://signer.dev.simpl-europe.eu/signer/v1/credential', {
    method: 'POST',
    body: JSON.stringify({
      context: ['https://w3id.org/security/suites/jws-2020/v1'],
      credentialSubject: json,
      issuer: 'did:web:did.dev.simpl-europa.eu',
      key: 'gaia-x-key1',
      namespace: 'transit',
      group: 'simpl',
    }),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  }).then((res) => res.json());
};

/**
 * sign json and upload to catalogue
 */
export const POST: APIRoute = async ({ request }) => {
  const bearerToken = request.headers.get('Authorization');
  const token = bearerToken?.split('Bearer ')[1];

  if (!bearerToken || !token)
    return new Response(JSON.stringify({ error_description: 'Invalid token' }), {
      status: 401,
    });

  const data = await request.json();
  console.log('DATA:', data);
  const signed = await signJson(data);
  console.log('SIGNED:', JSON.stringify(signed, null, 2));

  const response = await uploadSelfDescription(signed, token);
  console.log('RESPONSE:', response);
  return response;
};
