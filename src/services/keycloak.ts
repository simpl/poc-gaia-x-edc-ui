const AUTH_KEYCLOAK_ID = process.env.AUTH_KEYCLOAK_ID;
const AUTH_KEYCLOAK_SECRET = process.env.AUTH_KEYCLOAK_SECRET;
const AUTH_KEYCLOAK_ISSUER = process.env.AUTH_KEYCLOAK_ISSUER;

// serveer-side only accessible env variables

const requestHeaders = new Headers({
  'Content-Type': 'application/x-www-form-urlencoded',
  Accept: 'application/json',
});

export const checkSession = async (token: string) => {
  const requestBody = new URLSearchParams({
    client_id: AUTH_KEYCLOAK_ID,
    token: token,
    client_secret: AUTH_KEYCLOAK_SECRET,
  });

  const requestOptions: RequestInit = {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
    }),
    body: requestBody,
  };

  try {
    const response = await fetch(
      `${AUTH_KEYCLOAK_ISSUER}/protocol/openid-connect/token/introspect`,
      requestOptions
    );

    const data = await response.json();
    if (data.sub && data.active) {
      return true;
    }
  } catch (err) {
    return false;
  }
  return false;
};

export const login = async (username: string, password: string) => {
  const requestBody = new URLSearchParams({
    grant_type: 'password',
    client_id: AUTH_KEYCLOAK_ID,
    username,
    password,
    client_secret: AUTH_KEYCLOAK_SECRET,
  });

  const requestOptions: RequestInit = {
    method: 'POST',
    headers: requestHeaders,
    body: requestBody,
    redirect: 'follow',
    credentials: 'include',
  };

  return fetch(`${AUTH_KEYCLOAK_ISSUER}/protocol/openid-connect/token`, requestOptions);
};
