import { getPublicEnv } from '@/util/getEnv';
import type { AccessPoliciesDTO } from 'types/accessPolicy';

const { PUBLIC_CREATION_WIZARD_API_URL } = getPublicEnv();

export const getAccessPolicyJsonLd = async (accessPolicies: AccessPoliciesDTO) => {
  const response = await fetch(`${PUBLIC_CREATION_WIZARD_API_URL}/api/policy/access`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    body: JSON.stringify(accessPolicies),
    credentials: 'omit',
  });

  const data = await response.json();
  if (response.status === 200) {
    return data;
  } else {
    throw new Error();
  }
};
