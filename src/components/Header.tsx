import React, { useCallback } from 'react';

const navigationItems = [
  { path: '/', label: 'Home' },
  { path: '/search', label: 'Search' },
  { path: '/upload', label: 'Upload' },
  { path: '/login', label: 'Login' },
  { path: '/sign', label: 'Sign' },
];

const navigationListItems = navigationItems.map((item) => (
  <li key={item.path}>
    <a
      className="flex h-full flex-col justify-center px-4 hover:bg-ec-other-background hover:text-ec-dark hover:underline"
      href={item.path}
    >
      {item.label}
    </a>
  </li>
));

const HeaderComponent: React.FC<{ title: string; searchValue: string }> = ({
  title = 'SIMPL',
  searchValue,
}) => {
  const [search, setSearch] = React.useState(searchValue);

  const handleKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
        const url = new URL('/quicksearch', window.location.href);
        url.searchParams.set('search', search);
        window.location.href = url.toString();
      }
    },
    [search]
  );

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setSearch(e.target.value);
    },
    [setSearch]
  );

  return (
    <header className="bg-simpl-dark-blue flex items-center justify-between px-8 text-white">
      <div className="flex items-center py-4">
        <img src="/simpl-logo.png" alt="Logo" className="h-8 w-8" />
        <h1 className="ml-2 text-xl font-bold">{title}</h1>
      </div>
      <nav className="flex self-stretch">
        <ul className="flex flex-row justify-center">
          {navigationListItems}
          <li className="flex flex-row items-center">
            {/* search bar, vertical align center */}
            <input
              key="/quicksearch"
              type="text"
              className="h-8 w-48 rounded-md border-2 border-ec-dark p-2 text-black"
              placeholder="Search"
              value={search}
              onChange={handleChange}
              onKeyDown={handleKeyDown}
            />
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default HeaderComponent;
