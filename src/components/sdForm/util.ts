import { convertSchema } from '@/api/localApi';
import { getSchemas } from '@/services/federatedCatalogue';
import type { JsonSchema } from '@jsonforms/core';
import { ref, toValue, watchEffect, type Ref, type UnwrapRef } from 'vue';

export const useShapes = () => {
  const shapes = ref<string[]>([]);
  const isLoading = ref(false);
  const error = ref<string | null>(null);

  const fetchShapes = async () => {
    isLoading.value = true;
    try {
      const response = await getSchemas();
      shapes.value = response.shapes;
    } catch (err) {
      error.value = 'Failed to fetch shapes';
      console.error(err);
    } finally {
      isLoading.value = false;
    }
  };

  fetchShapes();

  return {
    data: shapes,
    isLoading,
    error,
    refresh: fetchShapes,
  };
};

export function useFetch<T, U>(fetchFunction: (params?: U) => Promise<T>, params?: Ref<U>) {
  const data = ref<T | null>(null);
  const isLoading = ref(false);
  const error = ref<string | null>(null);

  const fetchData = () =>
    fetchFunction(toValue(params))
      .then((res) => {
        data.value = res as UnwrapRef<T>;
      })
      .catch((err) => (error.value = err))
      .finally(() => (isLoading.value = false));

  watchEffect(async () => {
    await fetchData();
  });

  return {
    data,
    isLoading,
    error,
    refresh: fetchData,
  };
}

export type ShapeData = {
  root: {
    [key: string]: JsonSchema;
  };
  prefixes: Record<string, string>;
};

export const useShapeData = (schemaId: Ref<string | undefined>) => {
  const shapeData = ref<ShapeData | null>(null);
  const isLoading = ref(false);
  const error = ref<string | null>(null);

  const fetchShapeData = async (sid?: string) => {
    if (!sid) {
      return;
    }
    isLoading.value = true;

    try {
      const data = await convertSchema(sid);
      shapeData.value = data;
    } catch (err) {
      error.value = `Failed to fetch data for shape: ${schemaId}`;
      console.error(err);
    } finally {
      isLoading.value = false;
    }
  };

  watchEffect(async () => {
    await fetchShapeData(schemaId.value);
  });

  return {
    data: shapeData,
    isLoading,
    error,
    refresh: fetchShapeData,
  };
};
