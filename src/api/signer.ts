export const signJson = async (json: object) => {
  const response = await fetch('/signjson', {
    method: 'POST',
    body: JSON.stringify(json),
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
  const data = await response.json();
  if (data.kind && data.message) throw new Error(data.message);
  return data;
};
