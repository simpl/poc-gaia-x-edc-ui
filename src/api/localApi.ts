import type { NodeObject } from 'jsonld';

const getKeycloakToken = async () => {
  const token = window.localStorage.getItem('KEYCLOAK_ACCESS_TOKEN');
  if (!token) {
    throw new Error('No token found');
  }
  return token;
};

export const convertSchema = async (schemaId: string) => {
  const token = await getKeycloakToken();
  const response = await fetch(`/convert?schemaId=${schemaId}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'text/turtle',
    },
  });
  return response.json();
};

export const publishSelfDescription = async (selfDescription: NodeObject) => {
  const token = await getKeycloakToken();
  const response = await fetch(`/publish`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(selfDescription),
  });
  return response.json();
};
